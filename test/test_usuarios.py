import sys, os
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime
import time

sys.path.append(os.path.join(os.path.dirname(__file__),'../'))
from main import app
from app.db.base import Base
from app.db.sgenerator import get_db
from app.security.hashing import Hash

db_path = os.path.join(os.path.dirname(__file__),'test.db')
db_uri = "sqlite:///{}".format(db_path)
SQLALQUEMY_DATABASE_URL = db_uri
engine_test = create_engine(SQLALQUEMY_DATABASE_URL, connect_args={"check_same_thread":False})
TestingSessionLocal = sessionmaker(bind=engine_test, autocommit=False,autoflush=False)
Base.metadata.create_all(bind=engine_test)

cliente = TestClient(app)

def override_get_db():
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()

app.dependency_overrides[get_db] = override_get_db

def insertar_usuario_prueba():
    password_hash = Hash.hash_password("asd.")
    engine_test.execute(
        f""" 
        INSERT INTO users(nombre, username, password, created_at, updated_at)
        VALUES
        ("jose", "jisc", "{password_hash}", "{datetime.now()}", "{datetime.now()}")
        """
    )
insertar_usuario_prueba()


def get_headers():
    ## generamos el login del usuario
    usuario_login = {
        "username":"jisc",
        "password":"asd."
    }
    
    response_token = cliente.post("/login/", data=usuario_login)
    assert response_token.status_code == 200
    assert response_token.json()["token_type"] == "bearer"
    
    return { "Authorization": f"Bearer {response_token.json()['access_token']}" }

def test_crear_usuario():
    time.sleep(2)
    usuario = {
        "nombre": "jose isaias",
        "username": "sotelo",
        "password": "123."
    }
    
    response = cliente.post("/users/", json=usuario)
    assert response.status_code == 401

    response = cliente.post("/users/", json=usuario, headers=get_headers())
    assert response.status_code == 200
    assert response.json()['id'] > 0
    

def test_obtener_usuarios():
    response = cliente.get("/users/")
    assert response.status_code == 401

    response = cliente.get("/users/", headers=get_headers())
    assert len(response.json()) == 2


def test_eliminar_usuario():
    response = cliente.delete("/users/1")
    assert response.status_code == 401

    response = cliente.delete("/users/1", headers=get_headers())
    print("test_eliminar_usuario: ")
    print(response)
    assert response.json()["response"] == "Se elimino el usuario con el ID: 1"
    response_user = cliente.get("/users/1",  headers=get_headers())
    assert response_user.json()["detail"] == "No existe el usuario con el ID 1"


def test_actualizar_usuario():
    usuario = {
        "username":"jose isaias actualizado"
    }
    response = cliente.put("/users/2", json=usuario)
    assert response.status_code == 401

    response = cliente.put("/users/2", json=usuario, headers=get_headers())
    assert response.json()["response"] == "Se actualizo el usuario ID[2] correctamente"
    response_user = cliente.get("/users/2", headers=get_headers())
    assert response_user.json()["username"] == usuario["username"]


def test_no_encuentra_usuario():
    usuario = {
        "username":"jose isaias actualizado"
    }
    response = cliente.put("/users/12", json=usuario)
    assert response.status_code == 401

    response = cliente.put("/users/12", json=usuario, headers=get_headers())
    assert response.json()["detail"] == "No existe el usuario con el ID 12"

def test_delete_database():
    db_path = os.path.join(os.path.dirname(__file__),'test.db')
    os.remove(db_path)
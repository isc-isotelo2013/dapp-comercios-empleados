"""change relationship Comercio-Empleado

Revision ID: 41f21c29a69b
Revises: 9380ddc14334
Create Date: 2022-11-18 21:17:00.557148

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '41f21c29a69b'
down_revision = '9380ddc14334'
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass

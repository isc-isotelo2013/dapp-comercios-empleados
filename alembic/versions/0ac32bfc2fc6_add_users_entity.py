"""add users entity

Revision ID: 0ac32bfc2fc6
Revises: 41f21c29a69b
Create Date: 2022-11-18 22:55:48.131494

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0ac32bfc2fc6'
down_revision = '41f21c29a69b'
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass

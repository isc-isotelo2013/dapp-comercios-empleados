# Prueba de ingreso

<p align="center">
    <em> Web service para comercios - empleados </em>
</p>

---

## Requisitos previos

- python 3.9 +
- poetry
- alembic
- uvicorn
- virtualenv
- Tener libre el puerto 3000

--- 
## Configurar el ambiente

### Instalar virtualenv

    pip install virtualenv

### Genera el ambiente en el repositorio

    virtualenv venv

### Activa el ambiente (Windows)

    Debe ejecutar el script con el nombre "activate", este se encuentra en la carpeta generada en el paso anterior con el nombre "venv" dentro de la url: 
    
    > .\venv\Scripts\activate

    WINDOWS: Quizas reuqiera de darle permisos a powershell de ejecutar scripts:

    > Get-ExecutionPolicy
    > Set-ExecutionPolicy Unrestricted

---

## Instalación de dependencias

Instale poetry con pip

    pip install poetry
    poetry --version

Utlice poetry para la instalación de las dependencias

    poetry install

--- 

## Migraciones (OJO: Solo ejecute estos pasos si no se proporciono la DB SQLite)

Utilice alembic para la generación de migraciones

### Instalar el modulo:
	poetry add alembic
	
### inicializar alembic en el repo

	alembic init alembic
	
### generar la migraciones

	alembic revision --autogenerate -m "Initial migration."
	
### NOTA:
	Configure alembic env.py como por ejemplo sigue, configure los modelos a generar en target_metadata
	
	from app.db.base import Base
	from app.models.comercio import Comercio
	from app.models.empleado import Empleado
	target_metadata = [Base.metadata]
	
### Aplica las migraciones en la db

	alembic upgrade head
	

### Generar una segunda migracion

	alembic revision --autogenerate -m "change relationship Comercio-Empleado"

---

## Ejecución

### Use el comando:

    python main.py

    navegar a la dirección 127.0.0.1:3000/docs

---
## Documentación API

    Con el projecto en ejecución ir a:

    http://127.0.0.1:3000/docs#/


---
## Autenticación: 
    user: qwe
    password: qwe
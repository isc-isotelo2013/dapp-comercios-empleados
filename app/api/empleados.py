from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.schemas.empleado import Empleado, EmpleadoCreate, EmpleadoUpdate
from app.schemas.usuarios import Usuario
from app.security.hashing import Hash
from app.security.auth import get_current_user
from app.db.sgenerator import get_db
from app.repository.crud_empleado import empleado

router = APIRouter()

@router.get("/", response_model=List[Empleado], status_code=status.HTTP_200_OK)
async def get_empleados(
    *,
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    skip: int = 0,
    limit: int = 100,
    ) -> Any:
    """ Obtiene la lista de empleados """
    try:
        list = empleado.get_multi(db=db, skip=skip, limit=limit)
        return list
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )

@router.post("/", response_model=Empleado, status_code=status.HTTP_200_OK)
async def create_empleado(
    *,
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    new: EmpleadoCreate) -> Any:
    """ Registra un nuevo empleado """
    try:
        result = empleado.create(db=db, obj_in=new)
        if result:
            return result
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )

@router.put("/{id}", response_model=Empleado, status_code=status.HTTP_200_OK)
async def update_empleado(
    *,
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    id: int,
    update: EmpleadoUpdate) -> Any:
    """ Actualiza un empleados existente"""
    try:
        current = empleado.get(db, id)
        if not current:
            raise HTTPException(status_code=404, detail="Empleado no encontrado")
        current = empleado.update(
            db=db,
            db_obj=current,
            obj_in=update
        )
        return current
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )

@router.delete("/{id}", response_model=Empleado, status_code=status.HTTP_200_OK)
async def delete_empleado(
    *, 
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    id: int) -> Any:
    """ Elimina un empleados existente """
    try:
        current = empleado.get(db, id)
        if not current:
            raise HTTPException(status_code=404, detail="Empleado no encontrado")
        current = empleado.remove(db=db, id=id)
        return current
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )
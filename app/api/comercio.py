from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.schemas.comercios import Comercio, ComercioCreate, ComercioUpdate
from app.schemas.usuarios import Usuario
from app.security.hashing import Hash
from app.security.auth import get_current_user
from app.db.sgenerator import get_db
from app.repository.crud_comercio import comercio


router = APIRouter()

@router.get("/", response_model=List[Comercio], status_code=status.HTTP_200_OK)
async def get_comercios(
    *,
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    skip: int = 0,
    limit: int = 100,
    ) -> Any:
    """ Obtiene la lista de comercios """
    try:
        list = comercio.get_multi(db=db, skip=skip, limit=limit)
        return list
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )

@router.post("/", response_model=Comercio, status_code=status.HTTP_200_OK)
async def create_comercio(
    *,
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    new: ComercioCreate) -> Any:
    """ Registra un nuevo comercio """
    try:
        result = comercio.create(db=db, obj_in=new)
        if result:
            return result
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )

@router.put("/{id}", response_model=Comercio, status_code=status.HTTP_200_OK)
async def update_comercio(
    *,
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    id: int,
    update: ComercioUpdate) -> Any:
    """ Actualiza un comercio existente"""
    try:
        current = comercio.get(db, id)
        if not current:
            raise HTTPException(status_code=404, detail="Comercio no encontrado")
        current = comercio.update(
            db=db,
            db_obj=current,
            obj_in=update
        )
        return current
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )

@router.delete("/{id}", response_model=Comercio, status_code=status.HTTP_200_OK)
async def delete_comercio(
    *, 
    db:Session = Depends(get_db),
    current_user: Usuario = Depends(get_current_user),
    id: int) -> Any:
    """ Elimina un comercio existente """
    try:
        current = comercio.get(db, id)
        if not current:
            raise HTTPException(status_code=404, detail="Comercio no encontrado")
        current = comercio.remove(db=db, id=id)
        return current
    except Exception as e:
        raise HTTPException(
            status_code = status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error HTTP_500_INTERNAL_SERVER_ERROR {e}"
        )
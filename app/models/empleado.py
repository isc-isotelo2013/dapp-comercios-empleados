from sqlalchemy import Boolean, Column, Identity, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from uuid import uuid4
from app.db.base import Base

class Empleado(Base):
    __tablename__ = "empleado"
    id = Column(
        Integer,
        Identity(
            start=1,
            increment=1,
            minvalue=1,
            maxvalue=2147483647,
            cycle=False,
            cache=1,
        ),
        primary_key=True,
    )
    uuid = Column(String, default=str(uuid4()))
    nombre = Column(String(40))
    apellidos = Column(String(40))
    pin = Column(String(6))
    activo = Column(Boolean, default=True)
    # fecha_creacion = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP")) # default add created_at in base model
    comercio_id = Column(Integer, ForeignKey("comercio.id"))
    comercio = relationship("Comercio", back_populates="empleado")
from sqlalchemy import Boolean, Column, Identity, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from uuid import uuid4
from app.db.base import Base

class Comercio(Base):
    __tablename__ = "comercio"
    id = Column(
        Integer,
        Identity(
            start=1,
            increment=1,
            minvalue=1,
            maxvalue=2147483647,
            cycle=False,
            cache=1,
        ),
        primary_key=True,
    )
    uuid = Column(String, default=str(uuid4()))
    nombre = Column(String(100))
    activo = Column(Boolean, default=True)
    email_contacto = Column(String(50), nullable=True)
    telefono_contacto = Column(String(15), nullable=True)
    api_key = Column(String, default=str(uuid4()))
    # fecha_creacion = Column(DateTime, nullable=False, server_default=text("CURRENT_TIMESTAMP")) # default add created_at in base model
    empleado = relationship("Empleado", cascade="all, delete", back_populates="comercio")
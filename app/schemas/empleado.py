from typing import Any
from pydantic import BaseModel, Field
from pydantic.schema import Optional
from datetime import datetime
from uuid import UUID, uuid4

class EmpleadoBase(BaseModel):
    id: int
    uuid: str
    nombre: str
    apellidos: str
    pin: str
    activo: bool
    comercio_id: int
    comercio: Any
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True

class EmpleadoCreate(BaseModel):
    """ Empleado Create """
    uuid: UUID = Field(default_factory=uuid4)
    nombre: str
    apellidos: str
    pin: str
    activo: bool
    comercio_id: int

class EmpleadoUpdate(BaseModel):
    """ Empleado Update """
    nombre: str
    apellidos: str
    pin: str
    activo: bool
    comercio_id: int

class Empleado(EmpleadoBase):
    """ Empleado """
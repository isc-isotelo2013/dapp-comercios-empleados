from typing import Optional
from pydantic import BaseModel

class Response(BaseModel):
    is_success: bool = True
    error_message: Optional[str]
    message: Optional[str]
from typing import Any
from pydantic import BaseModel, Field
from pydantic.schema import Optional
from datetime import datetime
from app.schemas.empleado import Empleado
from uuid import UUID, uuid4

class ComercioBase(BaseModel):
    id: int
    uuid: str
    nombre: str
    activo: bool
    email_contacto: str
    telefono_contacto: str
    api_key: str
    created_at: datetime
    updated_at: datetime
    empleado: list[Any]
    
    class Config:
        orm_mode = True
        
class ComercioCreate(BaseModel):
    """ Comercio Create """
    uuid: UUID = Field(default_factory=uuid4)
    nombre: str
    activo: bool
    email_contacto: str
    telefono_contacto: str
    api_key: UUID = Field(default_factory=uuid4)

class ComercioUpdate(BaseModel):
    """ Comercio Update """
    nombre: str
    activo: bool
    email_contacto: str
    telefono_contacto: str

class Comercio(ComercioBase):
    """ Comercio """
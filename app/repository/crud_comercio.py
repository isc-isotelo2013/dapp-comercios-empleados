from app.repository.crud_base import CRUDBase
from app.models.comercios import Comercio
from app.schemas.comercios import ComercioCreate, ComercioUpdate

class CRUDComercio(CRUDBase[Comercio, ComercioCreate, ComercioUpdate]):
    pass

comercio = CRUDComercio(Comercio)
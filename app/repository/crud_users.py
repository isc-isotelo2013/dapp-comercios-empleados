from app.repository.crud_base import CRUDBase
from app.models.usuarios import User
from app.schemas.usuarios import UsuarioCreate, UsuarioUpdate

class CRUDUser(CRUDBase[User, UsuarioCreate, UsuarioUpdate]):
    pass

usuario = CRUDUser(User)
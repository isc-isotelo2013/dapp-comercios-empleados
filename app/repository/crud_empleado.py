from app.repository.crud_base import CRUDBase
from app.models.empleado import Empleado
from app.schemas.empleado import EmpleadoCreate, EmpleadoUpdate

class CRUDEmpleado(CRUDBase[Empleado, EmpleadoCreate, EmpleadoUpdate]):
    pass

empleado = CRUDEmpleado(Empleado)
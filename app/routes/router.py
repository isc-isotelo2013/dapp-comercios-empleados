from fastapi import APIRouter, status

from app.api import healthcheck, comercio, empleados, auth, usuarios

"""
Application API routes
"""
api_router = APIRouter()

api_router.include_router(
    healthcheck.router,
    prefix="/healthcheck",
    tags=["Healthcheck"],
    responses={status.HTTP_404_NOT_FOUND: {"description": "Not found"}},
)

api_router.include_router(
    auth.router,
    prefix="/login",
    tags=["Login"],
    responses={status.HTTP_404_NOT_FOUND: {"description": "Not found"}},
)

api_router.include_router(
    usuarios.router,
    prefix="/users",
    tags=["Usuarios"],
    responses={status.HTTP_404_NOT_FOUND: {"description": "Not found"}},
)

api_router.include_router(
    comercio.router,
    prefix="/comercios",
    tags=["Comercios"],
    responses={status.HTTP_404_NOT_FOUND: {"description": "Not found"}},
)

api_router.include_router(
    empleados.router,
    prefix="/empleados",
    tags=["Empleados"],
    responses={status.HTTP_404_NOT_FOUND: {"description": "Not found"}},
)